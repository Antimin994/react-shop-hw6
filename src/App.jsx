import { Route, Routes, Link, Outlet } from "react-router-dom"
import {useState, useEffect} from "react";
import {Layout} from "./components/Layout/Layout"
import HomePage from "./components/Pages/HomePage"
import Favorite from "./components/Pages/Favorite/Favorite"
import Basket from "./components/Pages/Basket/Basket"
import {useDispatch,useSelector} from "react-redux"
import {selectApp, selectFavorites, selectBaskets} from "./store/selectors";



function App() {
	const [isModalImage, setIsModalImage] = useState(false);
	const [isModal, setIsModal] = useState(false);
	const store__ = useSelector(selectApp)
	const favorites = useSelector(selectFavorites)
	const baskets = useSelector(selectBaskets);
	const dispatch = useDispatch()

    const handleModalImage = () => setIsModalImage(!isModalImage);
	const handleModal = () => setIsModal(!isModal);

    
	return (
		<>
				<Routes>
				  <Route path="/" element={<Layout favorite={favorites} basket={baskets} />}>	
					<Route index element={<HomePage  handleModalImage={handleModalImage} isModalImage={isModalImage} />}/>
					<Route path="/favorite" element={<Favorite />}/>
					<Route path="/basket" element={<Basket handleModal={handleModal} isModal={isModal} />}/>
				  </Route>
				</Routes>
			
		</>	
		
	)
}

export default App
