import {  render } from "@testing-library/react";
// import renderer from "react-test-renderer";
import "@testing-library/jest-dom/extend-expect";
import Button from "./Button.jsx";


describe("Button", () => {
  test("delete button snapshot", () => {
    const button = render(<Button  />);
    expect(button).toMatchSnapshot();
  });
  test("should have the correct type", () => {
    //const button = render(<Button  />);
    const type = "button";
    const { getByRole } = render(<Button type="button" />);
    const button = getByRole('button');
    expect(button).toHaveAttribute('type', type);
  });

});
