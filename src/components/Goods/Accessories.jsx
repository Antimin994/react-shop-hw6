import {Swiper,SwiperSlide} from 'swiper/react';
import "swiper/css"
import GoodsItem from './GoodsItem';
import { useContext } from "react";
import { ToggleContext } from "../../providers/ToggleContext";
import cx from "classnames";


const Accessories = ({date, isModalImage, handleModalImage})=>{
    const isToggled = useContext(ToggleContext).isToggled;console.log(isToggled);
    const goodsItem = date.map((item,index)=>(
    <SwiperSlide className="good__item">
        <GoodsItem item={item} key={index} handleModalImage={handleModalImage} isModalImage={isModalImage}/>
    </SwiperSlide>
    ))

    return(
        <Swiper
            slidesPerView={isToggled ? 1 : 5}
            spaceBetween={16}
            className={isToggled ? 'goods__table' : 'goods__wrapper'}
            direction={isToggled ? 'vertical' : 'horizontal'}
            navigation={true}
            grabCursor={false}
            draggable={false}
            autoplay={false}
            preventClicksPropagation={true}
            preventClicks={true}
            scrollbar={{draggable: false, hide: true}}
            slideToClickedSlide={false}
            pagination={{clickable: true}}
        >
         {goodsItem}
        </Swiper>
    )
}

export default Accessories
