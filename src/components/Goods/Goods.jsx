import Clothes from "./Clothes";
import Accessories from "./Accessories";
import {Link} from "react-router-dom";
import {sendRequest} from "../../helpers/sendRequest";
import {useEffect, useState} from "react";
import './Goods.scss'
import { useDispatch, useSelector } from "react-redux";
import { actionFetchClothes, actionFetchAccessories } from "../../store/actions";


const Goods = ({isModalImage, handleModalImage}) => {
	
	const [isOpen,setIsOpen]=useState(false);
	const handleModal = () => setIsOpen(!isOpen)
	const handleCurrentPost = (cardPost) => setCurrentPost(cardPost)
	const dispatch = useDispatch();
	
	
	useEffect(() => {
		dispatch(actionFetchClothes());
		dispatch(actionFetchAccessories());		
	}, [dispatch])

	const clothes = useSelector(state => state.clothes);
	const accessories = useSelector(state => state.accessories);

	

	return (
		<div className="container">
			<div className="goods__container">
				<div className="goods__title">Одяг та взуття</div>
				<div className="goods__slider">
					<Clothes date={clothes} handleModalImage={handleModalImage} isModalImage={isModalImage}/>
					<p className="goods__link"><Link to="/clothes/all" className="good-link"></Link></p>
				</div>
				<div className="goods__title">Аксесуари</div>
				<div className="goods__slider">
					<Accessories date={accessories} handleModalImage={handleModalImage} isModalImage={isModalImage}/>
					<p className="goods__link"><Link to="/accessories/all" className="good-link"></Link></p>
				</div>
			</div>
		</div>
	)
}


export default Goods
