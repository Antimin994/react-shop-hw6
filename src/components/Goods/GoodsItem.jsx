import Button from "../Button/Button";
import { AiFillStar } from "react-icons/ai";
import { AiOutlineStar } from "react-icons/ai";
import {useEffect} from "react";
import cx from "classnames";
import { useDispatch, useSelector } from "react-redux";
//import { handleModalImage } from "../../store/actions";


const GoodsItem = ({item, isModalImage, handleModalImage})=>{
    const {good_img, title, original_title, price, id, original_name, name } = item
    const dispatch = useDispatch();
    const favorites = useSelector(state => state.favorites);
    const currentItem = useSelector(state => state.currentItem);
    //const isModalImage = useSelector(state => state.isModalImage);
    const handleFavorite = () => {
        const favoriteAction = (data) => {
          return {
            type: 'HANDLE_FAVORITE',
            payload: item,
          };
        };
        dispatch(favoriteAction('Some payload data'));
      };
      const handleBasket = () => {
        const basketAction = (data) => {
          return {
            type: 'HANDLE_BASKET',
            payload: item,
          };
        };
        dispatch(basketAction('Some payload data'));
      };
      

    const linkPath = original_title ? `/cloth/${id}` : `/accessory/${id}`
    let isAdded = favorites.some((favorite)=> favorite.id === item.id)
    
    return (
        <>
        <div className="good-desc">
            <img src={good_img} height={'376px'} alt={title ? title : name}/>
            <AiFillStar onClick={()=>dispatch(handleFavorite(item))} className={cx("good-desc__favorite", {'good-desc__favorite_hidden':(!isAdded)})} />
            <AiOutlineStar onClick={()=>dispatch(handleFavorite(item))}  className={cx("good-desc__favorite", {'good-desc__favorite_hidden':isAdded})} />
            <div className='good-desc-back'>
                <h3 className="good-desc__title">{title ? title : name}</h3>
                <p className="good-desc__subtitle"><i>Артикул: {original_name ? original_name:id}</i></p>
                <p className="good-desc__price">Ціна {price} грн</p>
                <div className="button__wrapper">
                    <Button underlineView onClick={() => {
                    handleModalImage()
					dispatch(handleBasket(item))
					} }>додати у кошик</Button>
                </div>
            </div>
        </div>
        </>
        
    )
}


export default GoodsItem
