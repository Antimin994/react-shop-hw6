import {  render } from "@testing-library/react";
// import renderer from "react-test-renderer";
import "@testing-library/jest-dom/extend-expect";
import ModalWrapper from './ModalWrapper';


describe("ModalWrapper", () => {
  test("delete button snapshot", () => {
    const wrapper = render(<ModalWrapper  />);
    expect(wrapper).toMatchSnapshot();
  });
  test("should have the correct status", () => {
    const expectedIsActive = false;
    render(<ModalWrapper isOpen={expectedIsActive} />);
    expect(expectedIsActive).toBe(false);
  });

});
