import react from 'react'
import cx from "classnames";
import PropTypes from 'prop-types';
import BasketItem from './BasketItem';
import "./Basket.scss"
import ModalBase from '../../Modal/ModalBase';
import { useDispatch, useSelector } from "react-redux";
import Form from '../../Form/Formset'



const Basket = (props) => {
   const {handleModal, isModal} = props;
   const dispatch = useDispatch();
   const baskets = useSelector(state => state.baskets);
   const currentItem = useSelector(state => state.currentItem);
   console.log(baskets);

   const handleConfirmed = () => {
    const confirmedAction = (data) => {
      return {
        type: 'HANDLE_CONFIRMED',
        payload: currentItem,
      };
    };
    dispatch(confirmedAction('Some payload data'));
  };





   let totalPrice = 0, quantity;
   const basketItem = baskets.map((basket, index)=>{
    console.log(currentItem);
    basket.quantity === undefined ? quantity = 1 : quantity = basket.quantity;
    totalPrice = totalPrice + parseFloat(basket.price) * parseFloat(quantity);

    return (
        <div className="basket__item">
            <BasketItem basket={basket} key={index} handleModal={handleModal} />
       </div>
    )       
    })

  return (
    <div className="container">
			<div className="basket__container">
				<div className="basket__title">Кошик</div>
				<div className="basket__wrapper">{basketItem}</div>
                <ModalBase  isOpen={isModal} handleClose={handleModal} handleOk={()=>{
                  handleModal()
                  dispatch(handleConfirmed())}}/>
                <div className="basket__total">Всього до сплати: {totalPrice} грн</div>
        <Form></Form>
			</div>
	</div>
  )
}

export default Basket