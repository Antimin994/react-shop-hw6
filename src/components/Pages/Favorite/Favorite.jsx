import react from 'react'
import cx from "classnames";
import PropTypes from 'prop-types';
import FavoriteItem from './FavoriteItem';
import ModalBase from '../../Modal/ModalBase';
import "./Favorite.scss";
import { useDispatch, useSelector } from "react-redux";


const Favorite = () => {
  const favorites = useSelector(state => state.favorites);

   const favoriteItem = favorites.map((favorite, index)=>{
    return (
        <div className="favorite__item">
            <FavoriteItem key={index} favorite={favorite} />
       </div>
    )       
    })


  return (
    <div className="container">
			<div className="favorite__container">
				<div className="favorite__title">Обране</div>
				<div className="favorite__wrapper">{favoriteItem}</div>
			</div>
	</div>
  )
}

export default Favorite