//import { handleModalImage } from "../../store/actions";
import Goods from "../Goods/Goods"
import ModalImage from "../Modal/ModalImage"
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from "react-redux";
import { ToggleProvider } from '../../providers/ToggleContext';
import ToggleSwitch from "../../providers/ToggleSwitch";


const HomePage = ({isModalImage, handleModalImage}) => {
 
 const dispatch = useDispatch();
 const currentItem = useSelector(state => state.currentItem);
 //const isModalImage = useSelector(state => state.isModalImage)
 const handleConfirmed = () => {
  const confirmedAction = (data) => {
    return {
      type: 'HANDLE_CONFIRMED',
      payload: currentItem,
    };
  };
  dispatch(confirmedAction('Some payload data'));
};

    return (
        <>
        <ToggleProvider>
          <ToggleSwitch />
          <Goods  handleModalImage={handleModalImage} isModalImage={isModalImage} />
          <ModalImage  isOpen={isModalImage} handleClose={handleModalImage} handleOk={()=>{
            handleModalImage()
            dispatch(handleConfirmed())}} />
        </ToggleProvider>
        </>
    )
}


export default HomePage