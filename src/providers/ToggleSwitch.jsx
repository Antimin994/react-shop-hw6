import React, { useContext } from 'react';
import { ToggleContext } from './ToggleContext';

const ToggleSwitch = () => {
  const { isToggled, toggle } = useContext(ToggleContext);

  return (
    <div>
      <label>
        <input type="checkbox" checked={isToggled} onChange={toggle} />
        {isToggled ? 'On' : 'Off'}
      </label>
    </div>
  );
};

export default ToggleSwitch;