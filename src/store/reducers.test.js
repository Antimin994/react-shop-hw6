import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './reducers';
import { handleFavorite } from './actions';


describe('myReducer', () => {
  let store;

  beforeEach(() => {
    store = configureStore({
        reducer: rootReducer
    });
  });
  it('should increment the counter', () => {
    store.dispatch(handleFavorite({
        "id" : "1011",
        "name" : "Фляга військова пластикова з чохлом Mil-Tac 1л",
        "price" : 600,
        "color" : "black",
        "good_img" : "../../../public/assets/flask.png"
        }));
    expect(store.getState()).toStrictEqual({
        favorites: [{
            "id" : "1011",
            "name" : "Фляга військова пластикова з чохлом Mil-Tac 1л",
            "price" : 600,
            "color" : "black",
            "good_img" : "../../../public/assets/flask.png"
            }],
        baskets: [],
        currentItem: [],
        clothes: [],
        accessories: [],
        isModal: false,
        isModalImage: false,
        formData: {
            name: "Anton",
            surname: "Antimiychuk",
            age: "29",
            adress: 'Dnipro',
            email: "anton.antimiychuk@gmail.com",
            phone: ''
        }
    });
  });
});