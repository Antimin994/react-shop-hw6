import {selectFavorites, selectBaskets, selectCurrentItem, selectFromData} from './selectors';

describe('redux selectors', () => {
    it('select favorites', () => {
        const favorites = [{}];
        expect(selectFavorites({favorites})).toEqual(favorites);
    });
    it('select baskets', () => {
        const baskets = [{}];
        expect(selectBaskets({baskets})).toEqual(baskets);
    });
    it('select current', () => {
        const currentItem = [{}];
        expect(selectCurrentItem({currentItem})).toEqual(currentItem);
    });
    it('select form', () => {
        const formData = [{name: "Anton", surname: "Antimiychuk", age: "29", adress: 'Dnipro', email: "anton.antimiychuk@gmail.com", phone: ''}];
        expect(selectFromData({formData})).toEqual(formData);
    });
    
})